package com.btu.profilepage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        init()
        pass()
     }

    private fun init() {
        Glide.with(this)
            .load("https://cdn2.iconfinder.com/data/icons/user-icon-2-1/100/user_5-15-512.png")
            .into(profileImage)
        Glide.with(this)
            .load("https://images.pexels.com/photos/255379/pexels-photo-255379.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500")
            .placeholder(R.mipmap.ic_launcher)
            .into(coverImage)


    }

    private fun pass() {
        emailtextview.text = intent.extras?.getString("emailll", " ")
        passwordemail.text = intent.extras?.getString("password", " ")
    }
}
