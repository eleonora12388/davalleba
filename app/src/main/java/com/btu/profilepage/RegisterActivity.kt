package com.btu.profilepage

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        auth = FirebaseAuth.getInstance()
        signup.setOnClickListener {
            signupuser()
        }


    }

    private fun signupuser() {
        if (signupemail.text.toString().isEmpty()) {
            Toast.makeText(this, "Enter Email", Toast.LENGTH_SHORT).show()
            signupemail.requestFocus()

        }
        if (!Patterns.EMAIL_ADDRESS.matcher(signupemail.text.toString()).matches()) {
            signupemail.error = "Please enter valid email"
            signupemail.requestFocus()
            return
        }
        if (signuppassword.text.toString().isEmpty()) {
            Toast.makeText(this, "Enter Password", Toast.LENGTH_SHORT).show()
            signuppassword.requestFocus()

        }
        auth.createUserWithEmailAndPassword(
            signupemail.text.toString(),
            signuppassword.text.toString()
        )
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    startActivity(Intent(this, ProfileActivity::class.java))
                } else {
                    Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
                }
            }
    }
}
