package com.btu.profilepage

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        register()


    }

    private fun init() {
        auth=FirebaseAuth.getInstance()
        readData()
        loginButton.setOnClickListener {
            if (emailText.text.isNotEmpty() && password.text.isNotEmpty()) {
                login()

            } else {
                Toast.makeText(this, "Please fill all fields", Toast.LENGTH_LONG).show()
            }
        }


    }


    private fun login() {
        auth.signInWithEmailAndPassword(emailText.text.toString(), password.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {


                    saveUserData()
                    Log.d("login", "signInWithEmail:success")
                    val user = auth.currentUser
                    val intent = Intent(this, ProfileActivity::class.java)
                    intent.putExtra("email", emailText.text.toString())
                    intent.putExtra("password", password.text.toString())
                    startActivity(intent)
                } else {

                    Log.w("login", "signInWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()

                }


            }

    }


    private fun register() {
        register.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

    }
    private fun saveUserData(){
        val sharedPreferences=getSharedPreferences("user_data", Context.MODE_PRIVATE)
        val edit=sharedPreferences.edit()
        edit.putString("email",emailText.text.toString())
        edit.putString("password",password.text.toString())
        edit.commit()

    }
    private fun readData(){
        val sharedPreferences=getSharedPreferences("user_data", Context.MODE_PRIVATE)
        emailText.setText(sharedPreferences.getString("email",""))
        password.setText(sharedPreferences.getString("password",""))

    }
}
